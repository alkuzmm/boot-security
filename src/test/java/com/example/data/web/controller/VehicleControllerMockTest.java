package com.example.data.web.controller;

import com.example.data.entity.Manufacturer;
import com.example.data.entity.Vehicle;
import com.example.exception.ApplicationGlobalException;
import com.example.service.ManufacturerService;
import com.example.service.VehicleService;
import org.junit.jupiter.api.Test;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class VehicleControllerMockTest extends ControllerBaseTest {

    @MockBean
    private VehicleService vehicleService;

    @MockBean
    private ManufacturerService manufacturerService;

    @Test
    void whenEmptyVehicleListShouldRespondOkTest() throws Exception {
        //when
        when(vehicleService.getAllVehicles()).thenReturn(List.of());

        mockMvc.perform(get("/vehicles")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(0)))
                .andExpect(status().isOk());
    }

    @Test
    void whenListOfVehicleListShouldRespondOkAndReturnListTest() throws Exception {

        Vehicle vehicle = Vehicle.builder()
                .id(1L)
                .engineCapacity(3.5)
                .seats(2)
                .wheels(6)
                .vinNumber("XXXX")
                .vehicleMass(8300.0)
                .color("Black")
                .model("X8")
                .manufacturer(
                        Manufacturer.builder()
                                .address("Munich")
                                .companyName("BMW")
                                .carModelName("BMW")
                                .foundationYear(2020)
                                .id(1L)
                                .build()
                ).build();

        when(vehicleService.getAllVehicles()).thenReturn(List.of(vehicle));

        mockMvc.perform(get("/vehicles")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].engineCapacity", is(3.5)))
                .andExpect(jsonPath("$[0].seats", is(2)))
                .andExpect(jsonPath("$[0].wheels", is(6)))
                .andExpect(jsonPath("$[0].vinNumber", is("XXXX")))
                .andExpect(jsonPath("$[0].vehicleMass", is(8300.0)))
                .andExpect(jsonPath("$[0].color", is("Black")))
                .andExpect(jsonPath("$[0].model", is("X8")))
                .andExpect(jsonPath("$[0].manufacturer", is("BMW")))
                .andDo(print());

    }

    @Test
    void whenCreateVehicleShouldReturnCreatedTest() throws Exception {

        Manufacturer manufacturer = Manufacturer.builder()
                .id(1L)
                .address("Munich")
                .companyName("BMW")
                .carModelName("BMW")
                .foundationYear(2020)
                .build();

        Vehicle vehicle = Vehicle.builder()
                .id(1L)
                .engineCapacity(3.5)
                .seats(2)
                .wheels(6)
                .vinNumber("XXXX")
                .vehicleMass(8300.0)
                .color("Black")
                .model("X8")
                .manufacturer(manufacturer)
                .build();

        when(manufacturerService.findById(1L)).thenReturn(manufacturer);
        doAnswer((Answer<Void>) invocation -> null).when(vehicleService).save(vehicle);

        mockMvc.perform(post("/vehicles/manufacturers/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(vehicle)))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    void whenDeleteVehicleShouldReturnOkTest() throws Exception {

        Manufacturer manufacturer = Manufacturer.builder()
                .id(1L)
                .address("Munich")
                .companyName("BMW")
                .carModelName("BMW")
                .foundationYear(2020)
                .build();

        Vehicle vehicle = Vehicle.builder()
                .id(1L)
                .engineCapacity(3.5)
                .seats(2)
                .wheels(6)
                .vinNumber("XXXX")
                .vehicleMass(8300.0)
                .color("Black")
                .model("X8")
                .manufacturer(manufacturer)
                .build();

        doAnswer((Answer<Void>) invocation -> null).when(vehicleService).delete(vehicle);
        mockMvc.perform(delete("/vehicles/1"))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    void whenUpdateVehicleShouldReturnOkTest() throws Exception {

        Manufacturer manufacturer = Manufacturer.builder()
                .id(1L)
                .address("Munich")
                .companyName("BMW")
                .carModelName("BMW")
                .foundationYear(2020)
                .build();

        Vehicle vehicle = Vehicle.builder()
                .id(1L)
                .engineCapacity(4.5)
                .seats(2)
                .wheels(6)
                .vinNumber("XXXX")
                .vehicleMass(8300.0)
                .color("Black")
                .model("X8")
                .build();

        when(manufacturerService.findById(1L)).thenReturn(manufacturer);
        doAnswer((Answer<Void>) invocation -> null).when(vehicleService).update(vehicle);

        mockMvc.perform(put("/vehicles/update/manufacturer/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(vehicle)))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    void whenListOfVehiclesListShouldRespond500Test() throws Exception {

        when(vehicleService.getAllVehicles())
                .thenThrow(new ApplicationGlobalException());

        mockMvc.perform(get("/vehicles"))
                .andExpect(status().is(500));

    }
}
