package com.example.data.web.controller.integration;

import com.example.data.dao.ManufacturerDAO;
import com.example.data.entity.Manufacturer;
import com.example.data.web.controller.ControllerBaseTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class ManufacturerControllerIntegrationTest extends ControllerBaseTest {

    @Autowired
    ManufacturerDAO manufacturerDAO;

    @Test
    void whenEmptyManufacturersListShouldRespondOkTest() throws Exception {
        mockMvc.perform(get("/manufacturers")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(0)))
                .andExpect(status().isOk());

    }

    @Test
    @SqlGroup({
            @Sql(scripts = {"/insert_manufacturer.sql"})
    })
    @DisplayName("should return 'manufacturer with id 1 and HTTP 200'")
    void whenListOfManufacturersListShouldRespondOkAndReturnListTest() throws Exception {

        mockMvc.perform(get("/manufacturers")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].address", is("Berlin")))
                .andExpect(jsonPath("$[0].companyName", is("BMW")))
                .andExpect(jsonPath("$[0].carModelName", is("BMW")))
                .andExpect(jsonPath("$[0].foundationYear", is(1904)))
                .andExpect(jsonPath("$[0].id", is(1)));
    }

    @Test
    @DisplayName("should return 'HTTP 200'")
    void whenCreateManufacturerShouldReturnCreatedTest() throws Exception {

        Manufacturer expected = Manufacturer.builder()
                .address("Detroit")
                .companyName("Ford")
                .carModelName("Ford")
                .foundationYear(1903)
                .build();

        mockMvc.perform(post("/manufacturers")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(expected)))
                .andExpect(status().isCreated());

        List<Manufacturer> manufacturers = manufacturerDAO.findAll();

        assertEquals(1, manufacturers.size());
        Manufacturer actual = manufacturers.get(0);
        assertEquals(actual.getCompanyName(), expected.getCompanyName());
        assertEquals(actual.getCarModelName(), expected.getCarModelName());
        assertEquals(actual.getFoundationYear(), expected.getFoundationYear());
        assertEquals(actual.getAddress(), expected.getAddress());
        assertNotNull(actual.getId());
    }
}
