CREATE TABLE vehicle
(
    id              bigserial NOT NULL,
    color           character varying(255),
    engine_capacity double precision,
    model           character varying(255),
    seats           integer,
    mass            double precision,
    vin             character varying(255),
    wheels          integer,
    manufacturer_id bigint NOT NULL,
    CONSTRAINT vehicle_pkey PRIMARY KEY (id),
    CONSTRAINT uk_3vyjrop7rn1kcnfdhvlfthoc3 UNIQUE (vin),
    CONSTRAINT fkc6y2tuc57qy6qi28dqjeerodl FOREIGN KEY (manufacturer_id)
        REFERENCES manufacturer (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);