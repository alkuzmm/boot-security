CREATE TABLE user_account
(
    username varchar(255) NOT NULL,
    password varchar(255) NOT NULL,
    role     varchar(6)   NOT NULL
--     CONSTRAINT uk_user_account_username UNIQUE (username)
);

INSERT INTO user_account (username, password, role) VALUES ('user', 'user', 'USER');
INSERT INTO user_account (username, password, role) VALUES ('admin', 'admin', 'ADMIN');