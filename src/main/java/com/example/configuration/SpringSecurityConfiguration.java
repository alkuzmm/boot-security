//package com.example.configuration;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.http.HttpMethod;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//
//@EnableWebSecurity
//public class SpringSecurityConfiguration extends WebSecurityConfigurerAdapter {
//
//    @Autowired
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth
//                .inMemoryAuthentication()
//                .withUser("user").password("{noop}user").authorities("USER")
//                .and()
//                .withUser("admin").password("{noop}admin").authorities("USER", "ADMIN");
//    }
//
//    @Override
//    protected void configure(HttpSecurity httpSecurity) throws Exception {
//
//        httpSecurity
//                .httpBasic()
//                .and()
//                .authorizeRequests()
//                .antMatchers(HttpMethod.GET, "/vehicles/**").hasAnyAuthority("USER", "ADMIN")
//                .antMatchers(HttpMethod.POST, "/vehicles/**").hasAnyAuthority("USER", "ADMIN")
//                .antMatchers(HttpMethod.PUT, "/vehicles/**").hasAnyAuthority("USER", "ADMIN")
//                .antMatchers(HttpMethod.DELETE, "/vehicles/**").hasAnyAuthority("USER", "ADMIN")
//                .antMatchers(HttpMethod.GET, "/manufacturers/**").hasAnyAuthority("ADMIN")
//                .antMatchers(HttpMethod.POST, "/manufacturers/**").hasAnyAuthority("ADMIN")
//                .antMatchers(HttpMethod.PUT, "/manufacturers/**").hasAnyAuthority("ADMIN")
//                .antMatchers(HttpMethod.PATCH, "/manufacturers/**").hasAnyAuthority("ADMIN")
//                .antMatchers(HttpMethod.DELETE, "/manufacturers/**").hasAnyAuthority("ADMIN")
//                .and()
//                .csrf().disable()
//                .formLogin().disable();
//
//    }
//}
