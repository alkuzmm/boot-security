package com.example.configuration;

import com.example.service.auth.UserAuthenticationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SpringSecurityConfigurationDB extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserAuthenticationProvider authProvider;

    @Autowired
    public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authProvider);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .httpBasic()
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/vehicles/**").hasAnyAuthority("USER", "ADMIN")
                .antMatchers(HttpMethod.POST, "/vehicles/**").hasAnyAuthority("USER", "ADMIN")
                .antMatchers(HttpMethod.PUT, "/vehicles/**").hasAnyAuthority("USER", "ADMIN")
                .antMatchers(HttpMethod.DELETE, "/vehicles/**").hasAnyAuthority("USER", "ADMIN")
                .antMatchers(HttpMethod.GET, "/manufacturers/**").hasAnyAuthority("ADMIN")
                .antMatchers(HttpMethod.POST, "/manufacturers/**").hasAnyAuthority("ADMIN")
                .antMatchers(HttpMethod.PUT, "/manufacturers/**").hasAnyAuthority("ADMIN")
                .antMatchers(HttpMethod.PATCH, "/manufacturers/**").hasAnyAuthority("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/manufacturers/**").hasAnyAuthority("ADMIN")
                .and()
                .csrf().disable()
                .formLogin().disable();
    }
}
