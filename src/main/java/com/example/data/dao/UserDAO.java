package com.example.data.dao;

import com.example.data.entity.User;
import com.example.data.entity.Vehicle;

import java.util.List;
import java.util.Optional;

public interface UserDAO {

    Optional<User> getByUsernameAndPassword(String username, String password);

}
