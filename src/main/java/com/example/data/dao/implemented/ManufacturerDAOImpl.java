package com.example.data.dao.implemented;

import com.example.data.dao.ManufacturerDAO;
import com.example.data.entity.Manufacturer;
import com.example.data.repository.ManufacturerRepository;
import com.example.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ManufacturerDAOImpl implements ManufacturerDAO {

    private ManufacturerRepository manufacturerRepository;

    @Autowired
    public ManufacturerDAOImpl(ManufacturerRepository manufacturerRepository) {
        this.manufacturerRepository = manufacturerRepository;
    }

    @Override
    public Manufacturer findById(Long id) {
        return this.manufacturerRepository.findById(id).orElseThrow(()
                -> new NotFoundException(String.format("Manufacturer with id '%s' not found", id)));
    }

    @Override
    public Manufacturer save(Manufacturer manufacturer) {
        this.manufacturerRepository.save(manufacturer);
        return manufacturer;
    }

    @Override
    public List<Manufacturer> findAll() {
        return (ArrayList<Manufacturer>) this.manufacturerRepository.findAll();
    }

    @Override
    public Manufacturer findByName(String name) {
        return this.manufacturerRepository.findManufacturerByCompanyName(name);
    }
}
