package com.example.data.dao.implemented;

import com.example.data.dao.VehicleDAO;
import com.example.data.entity.Vehicle;
import com.example.data.repository.VehicleRepository;
import com.example.exception.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Transactional
@Repository("vehicleDAO")
@Slf4j
public class VehicleDAOImpl implements VehicleDAO {

    private VehicleRepository vehicleRepository;

    @Autowired
    VehicleDAOImpl(VehicleRepository vehicleRepository) {
        this.vehicleRepository = vehicleRepository;
    }

    @Override
    public Vehicle findById(Long id) {
        return vehicleRepository.findById(id).orElseThrow(() ->
                new NotFoundException(String.format("Vehicle with id '%s' not found", id)));
    }

    @Override
    public Vehicle save(Vehicle vehicle) {
        this.vehicleRepository.save(vehicle);
        return vehicle;
    }

    @Override
    public List<Vehicle> findAll() {
        return (List<Vehicle>) vehicleRepository.findAll();
    }

    @Override
    public void delete(Vehicle vehicle) {
        this.vehicleRepository.delete(vehicle);
    }

    @Override
    public void update(Vehicle vehicle) {
        this.vehicleRepository.save(vehicle);
    }
}
