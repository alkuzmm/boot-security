package com.example.data.dao.implemented;

import com.example.data.dao.UserDAO;
import com.example.data.entity.User;
import com.example.data.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UserDAOImpl implements UserDAO {

    private UserRepository userRepository;

    @Autowired
    UserDAOImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Optional<User> getByUsernameAndPassword(String username, String password) {
        return this.userRepository.findUserByUsernameAndPassword(username, password);
    }
}
