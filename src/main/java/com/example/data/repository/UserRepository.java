package com.example.data.repository;

import com.example.data.entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findUserByUsernameAndPassword(String username, String password);

}
