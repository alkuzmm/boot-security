package com.example.data.repository;

import com.example.data.entity.Manufacturer;
import org.springframework.data.repository.CrudRepository;

public interface ManufacturerRepository extends CrudRepository<Manufacturer, Long> {

    Manufacturer findManufacturerByCompanyName(String companyName);

}
