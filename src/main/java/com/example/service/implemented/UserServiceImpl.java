package com.example.service.implemented;

import com.example.data.dao.UserDAO;
import com.example.data.entity.User;
import com.example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserDAO userDAO;

    public UserServiceImpl(UserDAO userDAO) {
        this.userDAO = userDAO;
    }


    @Override
    public Optional<User> getByUsernameAndPassword(String username, String password) {
        return this.userDAO.getByUsernameAndPassword(username, password);
    }
}
