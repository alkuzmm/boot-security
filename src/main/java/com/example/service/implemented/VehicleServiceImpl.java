package com.example.service.implemented;

import com.example.data.dao.ManufacturerDAO;
import com.example.data.dao.VehicleDAO;
import com.example.data.entity.Manufacturer;
import com.example.data.entity.Vehicle;
import com.example.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class VehicleServiceImpl implements VehicleService {

    private final VehicleDAO vehicleDAO;
    private final ManufacturerDAO manufacturerDAO;

    @Autowired
    public VehicleServiceImpl(VehicleDAO vehicleDAO, ManufacturerDAO manufacturerDAO) {
        this.vehicleDAO = vehicleDAO;
        this.manufacturerDAO = manufacturerDAO;
    }

    @Override
    public List<Vehicle> getAllVehicles() {
        return this.vehicleDAO.findAll();
    }

    @Override
    public Vehicle save(Vehicle vehicle) {
        return vehicleDAO.save(vehicle);
    }

    @Override
    public void createVehicleForManufacturer(Long manufacturerId, Vehicle vehicle) {
        Manufacturer manufacturer = manufacturerDAO.findById(manufacturerId);
        manufacturer.getVehicles().add(vehicle);
        vehicle.setManufacturer(manufacturer);
        manufacturerDAO.save(manufacturer);
    }

    @Override
    public void delete(Vehicle vehicle) {
        vehicleDAO.delete(vehicle);
    }

    @Override
    public void update(Vehicle vehicle) {
        vehicleDAO.update(vehicle);
    }

    @Override
    public Vehicle findById(Long id) {
        return vehicleDAO.findById(id);
    }
}

