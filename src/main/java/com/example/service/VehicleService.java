package com.example.service;

import com.example.data.entity.Vehicle;

import java.util.List;

public interface VehicleService {

    List<Vehicle> getAllVehicles();

    Vehicle findById(Long id);

    Vehicle save(Vehicle vehicle);

    void createVehicleForManufacturer(Long manufacturerId, Vehicle vehicle);

    void delete(Vehicle vehicle);

    void update(Vehicle vehicle);
}
